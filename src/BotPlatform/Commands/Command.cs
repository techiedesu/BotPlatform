﻿using System;
using System.Threading.Tasks;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public abstract class Command<T> : ICommand where T : Command<T>, new()
    {
        protected static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new T();
                }
                return _instance;
            }
        }

        public abstract bool Init(object data);

        public abstract string[] EventCommands();

        public abstract string Exec(Message message);

        public virtual string ExecMarkdown(Message message) => "Dont";

        public virtual string Description() => string.Empty;

        public virtual string Author() => string.Empty;

        public virtual bool IsHidden() => false;

        public virtual string Help() => string.Empty;

        public virtual Task<string> ExecAsync(Message message)
        {
            return Task.Run(() => Exec(message));
        }

        public virtual int BootOrder() => 2;
    }
}
