﻿using System;
using BotPlatform.Helpers.Models;
using BotPlatform.OpenWeatherMap;

namespace BotPlatform.Commands
{
    public class WeatherCommand : Command<WeatherCommand>
    {
        protected Api Weather;

        public override bool Init(object data)
        {
            var apiKey = Environment.GetEnvironmentVariable("OPENWEATHERMAP_API_KEY");


            if (string.IsNullOrEmpty(apiKey))
            {
                return false;
            }

            Weather = new Api(apiKey);

            return Weather.IsAvailable();
        }

        public override string Description()
        {
            return "OpenWeatherMap provier.";
        }

        public override string Help()
        {
            return "enter `/<command> <city>` to get weather";
        }

        public override string Author()
        {
            return "V.A. (@techiedesu)";
        }

        public override string[] EventCommands()
        {
            return new[] { "weather" };
        }



        public override string Exec(Message message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);
            var city = "Novosibirsk";

            if (splitted.Length > 1)
            {
                city = splitted[1];
            }

            var curWeather = Weather.GetCityByName(city, "en");


            return new Pretty(curWeather).ToString();
        }
    }
}
