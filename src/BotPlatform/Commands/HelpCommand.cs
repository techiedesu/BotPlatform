﻿using System;
using System.Collections.Generic;
using System.Linq;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public class HelpCommand : Command<HelpCommand>
    {
        private List<ICommand> _commands = new List<ICommand>();

        public override bool Init(object data)
        {
            try
            {
                if (data != null)
                {
                    var inputCommands = (List<ICommand>) data;
                    Console.WriteLine("Registered command modules count: {0}", inputCommands.Count);
                    // _commands = inputCommands;
                    _commands = inputCommands.Where(x =>
                    {
                        Console.WriteLine("Command {1} - {0}", x.GetType(), !x.IsHidden() ? "in public list" : "hidden from pulic list");
                        return !x.IsHidden();
                    }).ToList();

                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public override string[] EventCommands()
        {
            return new[] { "help" };
        }

        public override string Description()
        {
            return "Like `man` just help";
        }

        public override string Help()
        {
            return "enter `/<command>` to get commands list";
        }

        public override string Author()
        {
            return "V.A. (@techiedesu)";
        }

        public override string Exec(Message message)
        {
            throw new NotImplementedException("Not today! Use markdown.");
        }

        public override string ExecMarkdown(Message message)
        {
            var result = "Commands list: " + Environment.NewLine;

            for (var i = 0; i < _commands.Count; i++)
            {
                var c = _commands[i];

                result += string.Format("{0}. {1} - {2}{3}", i + 1, string.Join(", ", c.EventCommands()), c.Help(), Environment.NewLine);
            }

            return result;
        }

        public override int BootOrder() => 5;
    }
}
