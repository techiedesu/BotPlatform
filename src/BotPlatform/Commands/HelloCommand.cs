﻿using System.Threading.Tasks;
using BotPlatform.Helpers;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public class HelloCommand : Command<HelloCommand>
    {
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "hi", "sup", "hai", "hello" };
        }

        public override string Description()
        {
            return "Just hello command.";
        }

        public override string Help()
        {
            return "enter `/<command>` to get beautiful answer!";
        }

        public override string Author()
        {
            return "V.A. (@techiedesu)";
        }

        public override string Exec(Message message)
        {
            var answerTo = "member";
            if (!string.IsNullOrEmpty(message.Sender.Username))
            {
                answerTo = $"@{message.Sender.Username}";
            }
            else if (!Common.IsNullOrEmpty(Logic.Or, message.Sender.FirstName, message.Sender.LastName))
            {
                answerTo = $"{message.Sender.FirstName} {message.Sender.LastName}";
            }
            else if (!string.IsNullOrEmpty(message.Sender.FirstName))
            {
                answerTo = message.Sender.FirstName;
            }

            var result = Common.WriteLine($"Hello, {answerTo}!");

            return result;
        }

    }
}
