﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    class PingCommand : Command<PingCommand>
    {
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "ping" };
        }

        public override string Description()
        {
            return "Ping reply checker.";
        }

        public override string Help()
        {
            return "enter `/<command> <host>` to check is alive host or not";
        }

        public override string Author()
        {
            return "V.A. (@techiedesu)";
        }

        public override string Exec(Message message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);
            var host = "mail.ru";

            if (splitted.Length > 1)
            {
                host = splitted[1];
            }

            return PingHost(host);
        }

        private static string PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.SendPingAsync(nameOrAddress).Result;
                pingable = reply.Status == IPStatus.Success;

                return  $"{reply.Address} is alive! Time: {reply.RoundtripTime} ms";
                ;
            }
            catch
            {
                // Discard PingExceptions and return false;
            }
            return "Host is dead";
        }
    }
}
