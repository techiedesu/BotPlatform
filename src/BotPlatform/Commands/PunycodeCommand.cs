﻿using System.Globalization;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public class PunycodeCommand : Command<PunycodeCommand>
    {
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "puny", "punycode" };
        }

        public override string Exec(Message message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);

            if (splitted.Length < 2) return "Wrong arguments.";

            var idn = new IdnMapping();

            return idn.GetAscii(splitted[1]) == splitted[1] 
                ? idn.GetUnicode(splitted[1]) : idn.GetAscii(splitted[1]);
        }

        public override string Description()
        {
            return "";
        }

        public override string Author()
        {
            return "V.A. (@techiedesu)";
        }

        public override string Help()
        {
            return "enter `/<command> <domain>` to get readble domain name or not";
        }
    }
}
