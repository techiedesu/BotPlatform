﻿using BotPlatform.Helpers.Models;
using CodeHollow.FeedReader;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Xml;

namespace BotPlatform.Commands
{
    public class NewsModule : Command<NewsModule>
    {
        protected class Channel
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            [JsonProperty("link")]
            public string Link { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("image")]
            public Image Image { get; set; }
            [JsonProperty("item")]
            public List<Item> Items { get; set; }
            [JsonProperty("lastBuildDate")]
            public string LastBuildDate { get; set; }
        }

        protected class Item
        {
            [JsonProperty("title")]
            public string Title { get; set; }
            [JsonProperty("link")]
            public string Link { get; set; }
            [JsonProperty("guid")]
            public string Guid { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("pubDate")]
            public string PubDate { get; set; }
        }

        protected class Image
        {
            [JsonProperty("url")]
            public string Url { get; set; }
            [JsonProperty("link")]
            public string Link { get; set; }
            [JsonProperty("title")]
            public string Title { get; set; }

        }

        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "news" };
        }

        public override string Exec(Message message)
        {
            var sb = new StringBuilder();
            var feed = FeedReader.ReadAsync("https://news.yandex.ru/society.rss").Result;



            sb.Append("Яндекс.Новости\r\n");
            if (feed.LastUpdatedDate != null)
            {
                sb.Append($"Последнее обновление: {((DateTime)feed.LastUpdatedDate).ToString("g", CultureInfo.CreateSpecificCulture("ru-RU"))} GTM\r\n\r\n"); 
                }

            var i = 1;
            foreach (var item in feed.Items)
            {
                if (i >= 5) break;
                sb.Append($"{i}. <a href=\"{item.Link}\">{item.Title}</a>\r\n");
                i++;

            }

            System.Console.WriteLine(sb.ToString());

            return sb.ToString();
        }



        public override bool IsHidden()
        {
            return true;
        }
    }
}
