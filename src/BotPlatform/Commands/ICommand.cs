﻿using System;
using System.Threading.Tasks;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public interface ICommand
    {
        bool Init(object data);
        string[] EventCommands();
        string Exec(Message message);
        string ExecMarkdown(Message message);
        string Description();
        string Author();
        int BootOrder();
        bool IsHidden();
        string Help();
        Task<string> ExecAsync(Message message);
    }
}
