﻿using System;
using System.Collections.Generic;
using System.Text;
using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public class RandomNumberCommand : Command<RandomNumberCommand>
    {
        readonly Random _random = new Random();
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] {"rand", "random"};
        }

        public override string Exec(Message message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);


            switch (splitted.Length)
            {
                case 2:
                {
                    int.TryParse(splitted[1], out var value1);
                    return _random.Next(value1).ToString();
                }
                case 3:
                {
                    int.TryParse(splitted[1], out var value1);
                    int.TryParse(splitted[2], out var value2);

                    return _random.Next(value1, value2).ToString();
                }
            }
            return _random.Next(int.MaxValue).ToString();
        }

        public override string Description()
        {
            return "Just simple random number generator.";
        }

        public override string Author()
        {
            return "V.A. (@techiedesu)";
        }

        public override string Help()
        {
            return "enter `/<command> <value_from> <value_to>` to get calculation result!";
        }
    }
}
