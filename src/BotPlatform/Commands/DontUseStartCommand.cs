﻿using BotPlatform.Helpers.Models;

namespace BotPlatform.Commands
{
    public class DontUseStartCommand : Command<DontUseStartCommand>
    {
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "start" };
        }

        public override string Exec(Message message)
        {
            return "Don't use this command nor suffix. Just type /help.";
        }

        public override bool IsHidden() => true;
    }
}
