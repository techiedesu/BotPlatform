﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using BotPlatform.Commands;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using BotPlatform.Helpers;

namespace BotPlatform
{
    public class  CarolineTelegramBot
    {
        protected readonly TelegramBotClient Bot;
        protected readonly int MyId;

        public List<ICommand> InterfaceBasedCommands { get; }

        /// <summary>
        /// Реализация бота для телеграма
        /// </summary>
        /// <param name="apiKey">Api ключ бота</param>
        public CarolineTelegramBot(string apiKey)
        {
            Bot = new TelegramBotClient(apiKey);
            MyId = Bot.GetMeAsync().Result.Id;

            InterfaceBasedCommands = new List<ICommand>();

            RegisterCommands();
            RegisterEvents();
            Console.WriteLine("Bot name is: @{0}", Bot.GetMeAsync().Result.Username);
            Bot.StartReceiving();
        }

        private void RegisterEvents()
        {
            Bot.OnMessage += async (o, s) => await _bot_OnMessageAsync(o ,s);
        }

        private async Task _bot_OnMessageAsync(object sender, MessageEventArgs e)
        {
            Console.WriteLine($"({e.Message.Chat.Id}) ({e.Message.From.Id}) {e.Message.Date} {e.Message.From.FirstName}: {e.Message.Text}");

            var isItMe = MyId == e.Message.From.Id;

            if (string.IsNullOrWhiteSpace(e.Message?.Text) || isItMe)
            {
                return;
            }

            await AnswerToMessageAsync(e.Message);
        }

        private Dictionary<string, ICommand> _handlers;

        private void RegisterCommands()
        {
            _handlers = new Dictionary<string, ICommand>();

            var commandTypes = AssemblyHelper.GetBasedTypes<ICommand>(Assembly.GetExecutingAssembly());
            var commandInstances = AssemblyHelper.GetTypeInstances<ICommand>(commandTypes);
            commandInstances = CommandHelper.CommandsByBootOrder(commandInstances);

            var commandInstancesActive = new List<ICommand>(commandInstances);

            foreach (var instance in commandInstances)
            {
                var instanceType = instance.GetType();
                var data = instanceType == typeof(HelpCommand) ? commandInstancesActive : null;

                var initResult = instance.Init(data);

                if (!initResult)
                {
                    Console.WriteLine("{0} failed to load. Skipping...", instanceType);
                    commandInstancesActive.Remove(instance);
                    continue;
                }
                Console.WriteLine("{0} has been loaded.", instanceType);
            }


            void OnSuccess(ICommand command, string trigger)
            {
                Console.WriteLine("Command `{0}` from `{1}` has been registered.", trigger, command.GetType());
            }

            void OnError(ICommand command, string trigger)
            {
                Console.WriteLine("Fatal error. Can't add command {0} from {1}", trigger, command.GetType());
            }

            _handlers = CommandHelper.GetCommandsCollection(commandInstancesActive, OnSuccess, OnError);
            Console.WriteLine("Commands have been registered");
        }

        private async Task AnswerToMessageAsync(Message tgMessage)
        {
            var message = Helpers.Converters.TelegramConverter.Message(tgMessage);

            var spilledStrMessage = message.Body.ToLower().Split(' ');
            var command = spilledStrMessage[0].Remove(0, 1);

            var username = "@" + (await Bot.GetMeAsync()).Username.ToLower();
            if (command.Contains(username))
            {
                command = command.Split('@')[0];
            }

            if (_handlers.ContainsKey(command))
            {
                var answer = _handlers[command].ExecMarkdown(message);
                var parseMode = ParseMode.Markdown;
                
                if(answer == "Dont" )
                {
                    answer = _handlers[command].Exec(message);
                    parseMode = ParseMode.Html;
                }

                if (string.IsNullOrWhiteSpace(answer)) return;
                try
                {

                    await Bot.SendTextMessageAsync(message.ChatId, answer, replyToMessageId: message.Id,
                        parseMode: parseMode);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Message {0} doesn't answered. Maybe deleted?", message.Id);

                    Console.WriteLine(ex);
                }
            }
        }

        public void Stop()
        {
            Bot.StopReceiving();
        }
    }
}
