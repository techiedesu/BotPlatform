﻿using System;
using System.Threading;

namespace BotPlatform
{
    internal class Program
    {
        [STAThread]
        private static void Main()
        {
            var tgApiKey = Environment.GetEnvironmentVariable("TELEGRAM_API_KEY");

            if (string.IsNullOrEmpty(tgApiKey))
            {
                while (true)
                {
                    Console.WriteLine("Check telegram key!"); 
                    Thread.Sleep(2000);
                }
            }

            Console.OutputEncoding = System.Text.Encoding.UTF8;

            // TODO: Return ManualResetEvent
            // ReSharper disable once UnusedVariable
            var carolineTelegramBot = new CarolineTelegramBot(tgApiKey);

            Console.WriteLine("Bot started");

            while (true)
            {
                Thread.Sleep(int.MaxValue); 
            }
        }

    }
}