﻿using System.Linq;
using RosBot.OpenWeatherMap.Models;

namespace BotPlatform.OpenWeatherMap
{
    public class Pretty
    {
        private readonly Answer _answer;

        public Pretty(Answer answer)
        {
            _answer = answer;
        }

        public string Icon()
        {
            switch (_answer.Weather.FirstOrDefault()?.Icon)
            {
                // clear sky
                case "01d":
                case "01n":
                    return "☀️";
                // few clounds
                case "02d":
                case "02n":
                    return "🌤";
                // scattered clouds
                case "03d":
                case "03n":
                    return "⛅️";
                // broken clouds
                case "04d":
                case "04n":
                    return "🌥";
                // shower rain
                case "09d":
                case "09n":
                    return "🌧";
                // rain
                case "10d":
                case "10n":
                    return "🌦";
                // thunderstorm
                case "11d":
                case "11n":
                    return "⛈";
                // snow
                case "13d":
                case "13n":
                    return "🌨";
                // mist
                case "50d":
                case "50n":
                    return "🌫";
            }

            return $"({_answer.Weather.FirstOrDefault()?.Description})";
        }

        public string Temp()
        {
            return $"{(int)(_answer.Main.Temp - 273.15)}";
        }

        public override string ToString()
        {
            try
            {
                var desc = _answer.Weather.FirstOrDefault()?.Description;

                if (desc != null && desc.Length > 1)
                {
                    desc = desc[0].ToString().ToUpper() + desc.Substring(1);
                }

                return$"City: {_answer.Name}\n" +
                      $"Main: {desc} {Icon()}\n"+ 
                      $"Temp: {Temp()} °C, {_answer.Main.Temp} K\n"+
                      $"Wind: {_answer.Wind.Speed} meter/sec";
            }
            catch
            {
                return "I've got wrong answer. Try later again.";
            }
        }
    }
}
