﻿using Newtonsoft.Json;

namespace RosBot.OpenWeatherMap.Models
{
    public class Clouds
    {
        /// <summary>
        /// Cloudiness, %
        /// </summary>
        [JsonProperty("all")]
        public int All { get; set; }
    }
}
