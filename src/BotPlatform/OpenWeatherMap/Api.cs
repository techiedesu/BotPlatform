﻿using System.Net.Http;
using System;
using Newtonsoft.Json;
using RosBot.OpenWeatherMap.Models;

namespace BotPlatform.OpenWeatherMap
{
    public class Api
    {
        readonly string _apiKey;

        /// <summary>
        /// OpenWeatherMap constructor.
        /// </summary>
        /// <param name="apiKey">You can get it here:
        /// https://home.openweathermap.org/api_keys</param>
        public Api(string apiKey)
        {
            _apiKey = apiKey;
        }

        public bool IsAvailable()
        {
            return !(string.IsNullOrEmpty(_apiKey) || GetCityByName("name", "ru") == null);
        }

        public Answer GetCityByName(string name, string lang)
        {
            var data = MakeRequest(
                $"http://api.openweathermap.org/data/2.5/weather?q=n{name}&APPID={_apiKey}&lang={lang}");
            try
            {
                return JsonConvert.DeserializeObject<Answer>(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex + "\n" + data);
                return null;
            }
        }

        protected string MakeRequest(string addr)
        {
            try
            {
                var hc = new HttpClient();
                var x = hc.GetStringAsync(addr).Result;

                return x;
            }
            catch
            {
                return null;
            }

        }
    }
}
