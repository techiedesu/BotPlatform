﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BotPlatform.Helpers
{
    public static class AssemblyHelper
    {
        public static List<Type> GetBasedTypes<T>(Assembly assembly)
        {
            var result = 
                assembly.GetTypes().Where(type =>
                    typeof(T).IsAssignableFrom(type) && !type.IsAbstract && !type.IsInterface);

            return result.ToList();
        }

        public static List<TBase> GetTypeInstances<TBase>(List<Type> typesList) where TBase : class
        {
            var instancesList = new List<TBase>();

            foreach(var currentType in typesList)
            {
                var newInstance = Activator.CreateInstance(currentType) as TBase;
                instancesList.Add(newInstance);
            }

            return instancesList;
        }
    }
}
