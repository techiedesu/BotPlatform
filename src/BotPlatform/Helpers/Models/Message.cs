﻿using System;

namespace BotPlatform.Helpers.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public User Sender { get; set; }
        public bool IsChat { get; set; }
        public DateTime PostTime { get; set; }
        public long ChatId { get; set; }
        public bool IsAnswer => AnswerTo != null;
        public Message AnswerTo { get; set; }
    }
}
