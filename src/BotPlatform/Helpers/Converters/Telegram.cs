﻿using BotPlatform.Helpers.Models;
using Telegram.Bot.Types.Enums;

namespace BotPlatform.Helpers.Converters
{
    public static class TelegramConverter
    {
        public static Message Message(Telegram.Bot.Types.Message tgMessage)
        {
            var message = new Message()
            {
                Id = tgMessage.MessageId,
                Body = tgMessage.Text,
                Sender = User(tgMessage.From),
                PostTime = tgMessage.Date,
                ChatId = tgMessage.Chat.Id,
                IsChat = tgMessage.Chat.Type != ChatType.Private,
                AnswerTo = new Message
                {
                    Sender = User(tgMessage.ForwardFrom)
                }
            };

            return message;
        }

        private static User User(Telegram.Bot.Types.User tgUser)
        {
            if (tgUser == null) return null;

            var user = new User
            {
                Id = tgUser.Id,
                FirstName = tgUser.FirstName,
                LastName = tgUser.LastName,
                Username = tgUser.Username
            };

            return user;
        }
    }
}
