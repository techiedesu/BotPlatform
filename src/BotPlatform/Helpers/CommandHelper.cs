﻿using BotPlatform.Commands;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BotPlatform.Helpers
{
    public class CommandHelper
    {
        public static List<ICommand> CommandsByBootOrder(List<ICommand> commandsList)
        {
            for (var i = 0; i < commandsList.Count; ++i)
            {
                commandsList.Sort((x, y) => x.BootOrder().CompareTo(y.BootOrder()));
            }
            return commandsList;
        }

        public static Dictionary<string, ICommand> GetCommandsCollection(List<ICommand> commandsList, Action<ICommand, string> onSuccess, Action<ICommand, string> onError)
        {
            var collection = new Dictionary<string, ICommand>();

            foreach(var command in commandsList)
            {
                foreach(var triggerCommand in command.EventCommands())
                {
                    if(!collection.ContainsKey(triggerCommand))
                    {
                        collection.Add(triggerCommand, command);
                        onSuccess(command, triggerCommand);
                    }
                    else
                    {
                        onError(command, triggerCommand);
                        return collection;
                    }
                }
            }

            return collection;
        }
    }
}
