﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BotPlatform.Helpers
{
    public static class Common
    {
        

        public static bool AnyContains(string where, params string[] args)
        {
            return args.Any(where.Contains);
        }



        public static bool IsUserAdmin(long chatid, long userid)
        {
            return (userid == 323831330);
        }

        public static string WriteLine(string text = "")
        {
            return text + "\n";
        }

        public static bool IsNullOrEmpty(Logic operation, params object[] args)
        {
            switch (operation)
            {
                case Logic.Or:
                    foreach (var arg in args)
                    {
                        if (arg is string s)
                        {
                            if (string.IsNullOrEmpty(s))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (arg == null)
                            {
                                return true;
                            }
                        }
                    }
                    break;
                case Logic.And:
                    throw new NotImplementedException();
            }

            return false;
        }

        public static string[] ParseCommandArgs(string rawString)
        {
            // TODO: Double quotes etc
            return rawString.Split(' ');
        }
    }
}
